package main.java.com.github.kuli.magenta;

import main.java.com.github.kuli.magenta.processable.Processable;
import main.java.com.github.kuli.magenta.processable.ProcessableBase;

import java.util.List;

/**
 * Created by Luca S. | kuli1230
 * Copyright (c) 2017 by author.
 * Twitter | @kuli1230
 */
public class Future<T> {

    private boolean done;
    private boolean success;

    private T result;

    private Throwable reason;
    private boolean cancelled;

    private Future<Object> thenFuture;
    private ProcessableBase thenProcessable;

    private List<Future<T>> siblings;

    public static <T> Future<T> createNull() {
        Future<T> future = new Future<>();
        future.resolve( null );
        return future;
    }
}
