package main.java.com.github.kuli.magenta.loader;

import java.net.URL;
import java.net.URLClassLoader;

/**
 * Created by Luca S. | kuli1230
 * Copyright (c) 2017 by author.
 * Twitter | @kuli1230
 */
class PluginClassLoader extends URLClassLoader {
    private static ClassLoader applicationClassLoader;

    static {
        ClassLoader.registerAsParallelCapable();
    }

    public PluginClassLoader( URL[] urls ) {
        super( urls );
    }

    @Override
    protected Class<?> loadClass( String name, boolean resolve ) throws ClassNotFoundException {
        return this.loadClass0( name, resolve, true );
    }

    private Class<?> loadClass0( String name, boolean resolve, boolean checkOther ) throws ClassNotFoundException {

        try {
            return super.loadClass( name, resolve );
        } catch ( ClassNotFoundException ignored ) {
            return applicationClassLoader.loadClass( name );
        }
    }
}

