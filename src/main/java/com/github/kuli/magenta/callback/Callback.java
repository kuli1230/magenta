package main.java.com.github.kuli.magenta.callback;

/**
 * Created by Luca S. | kuli1230
 * Copyright (c) 2017 by author.
 * Twitter | @kuli1230
 */
public interface Callback<T> {

    void invoke(T arg0);
}
