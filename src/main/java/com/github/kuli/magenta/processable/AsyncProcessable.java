package main.java.com.github.kuli.magenta.processable;

import java.util.concurrent.Future;

/**
 * Created by Luca S. | kuli1230
 * Copyright (c) 2017 by author.
 * Twitter | @kuli1230
 */
public interface AsyncProcessable<Input, Output> extends ProcessableBase {

    Future<Output> processAsync(final Input input);
}
