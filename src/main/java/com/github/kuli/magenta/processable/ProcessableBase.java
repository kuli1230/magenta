package main.java.com.github.kuli.magenta.processable;

/**
 * Created by Luca S. | kuli1230
 * Copyright (c) 2017 by author.
 * Twitter | @kuli1230
 */
public interface ProcessableBase {

    void fail(final Throwable reason);
}
